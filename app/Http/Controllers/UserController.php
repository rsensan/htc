<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use App;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::all();
        return response()->json($users);
    }

    public function getUser($id){

        $user  = User::find($id);

        return response()->json($user);
    }

    public function createUser(Request $request)
    {
    	global $app;

		$v = Validator::make($request->only('email', 'name', 'password'), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255', //|unique:users
            'password' => 'required|string|min:6',
        ]);

        if ($v->fails()) {
            return response()->json($v->errors()->all(), 400);
        }
        $data = $request->only('email','name','password');
        $password = $data['password'];
        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);
        //return response()->json($user);

        $client = \Laravel\Passport\Client::where('password_client', 1)->first();

        $tokenRequest = Request::create('/oauth/token', 'POST', [
            'grant_type'    => 'password',
            'client_id'     => $client->id,
            'client_secret' => $client->secret,
            'username'      => $data['email'],
            'password'      => $password,
            'scope'         => null,
        ]);

        return $app->dispatch($tokenRequest);

    }

    public function updateUser(Request $request, $id)
    {
        $user=User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->save();
        return response()->json($user);
    }

    public function deleteUser($id){
        $user  = User::find($id);
        $user->delete();
        return response()->json('deleted');
    }

}
?>