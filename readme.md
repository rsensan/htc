# Steps to install

* Clone repository git clone https://rsensan@bitbucket.org/rsensan/htc.git
* cd htc [folderpath']
* Composer install
* Go to http://testhtc.localhost:8000/ [location path, to check lumen installed correctly]
* copy .evn.example to .env and change your db details
	* DB_DATABASE=testhtc
	* DB_USERNAME=homestead
	* DB_PASSWORD=secret
* php artisan migrate
* php artisan passport:install
* php artisan db:seed
* Go to postman
	* http://testhtc.localhost:8000/user
		* simple form post with following details
			* name: senthil
			* email: rsensan@gmail.com
			* password: 123123 
		* you can get the token
			* {"token_type":"Bearer","expires_in":31536000,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImNiOGEwNDFmZTZlNjk3Y2JjYTIyZDI0N2I4ZGMzZTZmMWJhM2Q3NGMzNGQzMTJlYTBiZWEyM2JhOTExNzdjODk4MDA1MGRkNWI1YWNkYjViIn0.eyJhdWQiOiIyIiwianRpIjoiY2I4YTA0MWZlNmU2OTdjYmNhMjJkMjQ3YjhkYzNlNmYxYmEzZDc0YzM0ZDMxMmVhMGJlYTIzYmE5MTE3N2M4OTgwMDUwZGQ1YjVhY2RiNWIiLCJpYXQiOjE1MTUyMTM5NTUsIm5iZiI6MTUxNTIxMzk1NSwiZXhwIjoxNTQ2NzQ5OTU1LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.V0swfZhPmY0auDSj53uEG13iD7CO7B7S5fGYXbaDWYLvuzUjr_dJrJAfHRc0qAPpmpUqJgXUN1UF8pDbRYGnq6eC0kF1PcY89UGW_sEjQ9m-iEUV-RLYmbmz1jrXe5osVu9tJ_bYpsuSdefwxal_yw2m4hwFO67jdZSmgnpS0KniLgipNh9hwY-rqOztTme_wrWWkBSQOOCXJvGrS1N2M-TLWNjQk8WPNzzdQFj0Mb6aaV9MBVdLk-LakDeN5sznl_MY10aNbrLmztbJdU5BGpPIflyU78dbaMXC9emKMzLN6kLzCaH1EV8luIeCPzRPPZl0Eso9JLS5_eQSdETE-QCS4tBM6f8J6Na1wiL1zRJCdu5mGGAvGxTtTcD5OiOyOu-oZ7Ns4WnTCGP_V0rE2HXZYc4K_LnDEPktC_yoGIahuAliU9RonX2nxbwe7FJPjSoMlvIBy4x5ZbinN0xm8CpaeqOKZmJ3WX5iiJywE7GwCyYWssentqdaXkuWrlaAxZXHhcQ6RMk30Mc0IRD9Vq32YXBEq-9V4gfGuQOvpMostvszVC10PfuywxbGwBXtogaRsf-5v71dXjxNRt81lVGnHw0rPpt3PMb0Pc9zOMj9a5ELX5I4ofdTX5N_ySpbeUWGPJxYQh36rW6W4cxrcry3U2nCYwf_Il4xpXUDEts","refresh_token":"def50200937e034cc958e9198ddb181b03c2b11f2d2e64b363d0a85415d2ff03e0358f6439537917e0b9eb979556ca589b22f21dd11592463e2872b8770aeb8df090d06a4c2f5eed535f7d00bd1b417dcb5db798f9f70a54c5c9b6c74c9afc70ea2382a28c9a66e8481c05fae482f9a22b07a46d5d5069cd8ed355d8dc24b2ed8fe2e94ef51558b89a8fb0ea2218dfa7248c057b35a920fc44baaf265631823d8e30c696d5ad1c442c9f701cbab31f71ee905a46c34bcbb301c8a2088deb666203836d75987e5112aa900612f0df42528831c74ec0ee8965c9c39b5f309c93ea6d9756e4b6d149fbcc1a9337d587ad26c7cddc43e0b44a98e01f3e87d4f0bfb9378f8f923fc24f96c585ef906fbc2d6f3018fd380d8865ccca85f220ee56f45606db96e5c7755906eac5df3cb217e6bb90f2743fc621bab4aa617bc5e61d84b24c1ccc23fa2b1b28e1b2e5bfad464e7721723a46c5f3b2a71b22a293be1541"}
	* http://testhtc.localhost:8000/product
		* use the above "access_token" and pass in headers like
		* Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImNiOGEwNDFmZTZlNjk3Y2JjYTIyZDI0N2I4ZGMzZTZmMWJhM2Q3NGMzNGQzMTJlYTBiZWEyM2JhOTExNzdjODk4MDA1MGRkNWI1YWNkYjViIn0.eyJhdWQiOiIyIiwianRpIjoiY2I4YTA0MWZlNmU2OTdjYmNhMjJkMjQ3YjhkYzNlNmYxYmEzZDc0YzM0ZDMxMmVhMGJlYTIzYmE5MTE3N2M4OTgwMDUwZGQ1YjVhY2RiNWIiLCJpYXQiOjE1MTUyMTM5NTUsIm5iZiI6MTUxNTIxMzk1NSwiZXhwIjoxNTQ2NzQ5OTU1LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.V0swfZhPmY0auDSj53uEG13iD7CO7B7S5fGYXbaDWYLvuzUjr_dJrJAfHRc0qAPpmpUqJgXUN1UF8pDbRYGnq6eC0kF1PcY89UGW_sEjQ9m-iEUV-RLYmbmz1jrXe5osVu9tJ_bYpsuSdefwxal_yw2m4hwFO67jdZSmgnpS0KniLgipNh9hwY-rqOztTme_wrWWkBSQOOCXJvGrS1N2M-TLWNjQk8WPNzzdQFj0Mb6aaV9MBVdLk-LakDeN5sznl_MY10aNbrLmztbJdU5BGpPIflyU78dbaMXC9emKMzLN6kLzCaH1EV8luIeCPzRPPZl0Eso9JLS5_eQSdETE-QCS4tBM6f8J6Na1wiL1zRJCdu5mGGAvGxTtTcD5OiOyOu-oZ7Ns4WnTCGP_V0rE2HXZYc4K_LnDEPktC_yoGIahuAliU9RonX2nxbwe7FJPjSoMlvIBy4x5ZbinN0xm8CpaeqOKZmJ3WX5iiJywE7GwCyYWssentqdaXkuWrlaAxZXHhcQ6RMk30Mc0IRD9Vq32YXBEq-9V4gfGuQOvpMostvszVC10PfuywxbGwBXtogaRsf-5v71dXjxNRt81lVGnHw0rPpt3PMb0Pc9zOMj9a5ELX5I4ofdTX5N_ySpbeUWGPJxYQh36rW6W4cxrcry3U2nCYwf_Il4xpXUDEts
		* you can get the result. 


# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
