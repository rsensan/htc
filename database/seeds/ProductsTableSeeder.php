<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('product')->truncate();
    	$now = date('Y-m-d H:i:s');
    	DB::table('product')->insert([
            'name' => 'Product One 111',
            'details' => 'ok',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    	DB::table('product')->insert([
            'name' => 'Product Two 222',
            'details' => 'ok',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    	DB::table('product')->insert([
            'name' => 'Product Three 333',
            'details' => 'offline',
            'created_at' => $now,
            'updated_at' => $now
        ]);

    }
}
