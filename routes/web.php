<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function() {
    return str_random(32);
});

//CURD for user
$router->post('user','UserController@createUser');
$router->group(['middleware' => array('auth')], function () use($router){
	//CURD for user
	$router->get('user','UserController@index');
	$router->get('user/{id}','UserController@getUser');
	$router->post('user/{id}','UserController@updateUser');
	$router->delete('user/{id}','UserController@deleteUser');

	//CURD for product
	$router->get('product','ProductController@index');
	$router->get('product/{id}','ProductController@getProduct');
	$router->post('product','ProductController@createProduct');
	$router->post('product/{id}','ProductController@updateProduct');
	$router->delete('product/{id}','ProductController@deleteProduct');

});

